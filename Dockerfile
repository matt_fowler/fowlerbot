FROM python:3.6

ARG MIST_TOKEN
ARG ORG_ID
ARG SLACK_BOT_TOKEN

ENV INSTALL_PATH /fowlerbot
ENV MIST_TOKEN=$MIST_TOKEN
ENV ORG_ID=$ORG_ID
ENV SLACK_BOT_TOKEN=$SLACK_BOT_TOKEN

RUN mkdir -p $INSTALL_PATH
RUN mkdir -p $INSTALL_PATH/images

WORKDIR $INSTALL_PATH

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY fowlerbot.py fowlerbot.py

CMD ["python", "fowlerbot.py"]